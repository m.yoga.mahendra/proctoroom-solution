#!/bin/sh

# Setup Moodle

# Install Postgresql
sudo apt-get -y install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ 'lsb_release -cs'-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
sudo apt-get -y update
sudo apt-get -y install postgresql postgresql-contrib

# Download Adil Moodle
sudo docker pull registry.gitlab.com/aulia-adil/proctor-room

# Build Image yang telah disesuaikan dengan VM
cd ~/proctoroom-solution
sudo docker build --build-arg publicIpAddress=$(curl ifconfig.me) --build-arg dbhost='postgres' -t adil/moodle:v6 .

# Buat network Moodle
sudo docker network create -d bridge moodle-network

# Run postgresql terlebih dahulu sebelum Moodle
sudo docker run --name postgres -d -i -p 5431:5432 --network moodle-network --restart always --env POSTGRES_USER=moodleuser --env POSTGRES_PASSWORD=moodleuser debezium/postgres:12

# Run Moodle
cd
mkdir moodledata
sudo docker run \
 --name moodle -d -i -v moodledata:/var/moodledata \
  -p 80:80 -p 5000:5000 --network moodle-network \
  adil/moodle:v6

# Install Moodle
sudo docker exec -it moodle chown -R www-data /var/moodledata
sudo docker exec -it moodle chmod -R 777 /var/moodledata
sudo docker exec -it moodle chmod -R 777 /var/moodledata
sudo docker exec -it moodle sh install_moodle.sh
sudo docker exec -it moodle service apache2 start
sudo docker exec -it moodle nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > ~/websocket-output.log 2>&1 &
sudo docker exec -ti moodle sed -i "s|^.*examplepassword.*$|\$CFG->tool_generator_users_password = 'examplepassword';|" /var/www/html/moodle/config.php
sudo docker exec -ti moodle sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/7.4/cli/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/8.1/cli/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/7.4/apache2/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/7.4/cli/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/8.1/cli/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/7.4/apache2/php.ini
sudo docker exec -ti moodle service apache2 restart

# Setup debezium stacks
cd
git clone https://gitlab.com/m.yoga.mahendra/microservices-moodle.git
cd ~/microservices-moodle
sudo docker-compose -f debezium/docker-compose.yml up --force-recreate -d

# Buat bridge debezium
bash debezium/refresh-connector.sh