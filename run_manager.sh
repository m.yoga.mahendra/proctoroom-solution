#!/bin/sh

# Setup Docker Swarm Manager
docker swarm init

managertoken=`docker swarm join-token worker | grep docker`

echo $managertoken

# Setup Kafka Management Stacks
cd
git clone https://gitlab.com/m.yoga.mahendra/microservices-manager.git
cd microservices-manager

# Buat network overlay untuk warp-net
docker network create --driver=overlay --attachable warp-net
docker network create --driver=overlay --attachable moodle-network


# deploy stacks kafka brokers
docker stack deploy --compose-file docker-compose-kafka-1.yml kafka-1
docker stack deploy --compose-file docker-compose-kafka-2.yml kafka-2
docker stack deploy --compose-file docker-compose-kafka-3.yml kafka-3

