#!/bin/sh

sudo apt-get -y update

# Install semua tools umum
sudo apt-get -y install maven
sudo apt-get -y install vim
sudo apt-get -y install git

# Install docker
sudo apt-get -y update
sudo apt-get -y install     apt-transport-https     ca-certificates     curl     gnupg     lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get -y update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo service docker start
sudo curl -L \
    https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) \
    -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Install Postgresql
sudo apt-get -y install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ 'lsb_release -cs'-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
sudo apt-get -y update
sudo apt-get -y install postgresql postgresql-contrib

# Download Adil Moodle
sudo docker pull registry.gitlab.com/aulia-adil/proctor-room

# Download Hazman Scele Datastream System
cd
mkdir favian-scele-datastream-system
cd favian-scele-datastream-system
git clone https://gitlab.com/m.yoga.mahendra/favian-stacks-webserver.git
git clone https://gitlab.com/aulia-adil/favian-warp-kafka.git
git clone https://gitlab.com/aulia-adil/asynchronous-plugin-reader.git

# Build Image yang telah disesuaikan dengan VM
cd ~/proctoroom-solution
sudo docker build --build-arg publicIpAddress=$(curl ifconfig.me) --build-arg dbhost='postgres' -t adil/moodle:v6 .

# Buat network Moodle
sudo docker network create -d bridge moodle-network

# Run postgresql terlebih dahulu sebelum Moodle
sudo docker run --name postgres -d -i -p 5431:5432 --network moodle-network --restart always --env POSTGRES_USER=moodleuser --env POSTGRES_PASSWORD=moodleuser debezium/postgres:12

# Run Moodle
cd
mkdir moodledata
sudo docker run \
 --name moodle -d -i -v moodledata:/var/moodledata \
  -p 80:80 -p 5000:5000 --network moodle-network \
  adil/moodle:v6

# Install Moodle
sudo docker exec -it moodle chown -R www-data /var/moodledata
sudo docker exec -it moodle chmod -R 777 /var/moodledata
sudo docker exec -it moodle chmod -R 777 /var/moodledata
sudo docker exec -it moodle sh install_moodle.sh
sudo docker exec -it moodle service apache2 start
sudo docker exec -it moodle nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > ~/websocket-output.log 2>&1 &
sudo docker exec -it moodle sed -i "s|^.*examplepassword.*$|\$CFG->tool_generator_users_password = 'examplepassword';|" /var/www/html/moodle/config.php
sudo docker exec -it moodle sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/7.4/cli/php.ini
sudo docker exec -it moodle sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/8.1/cli/php.ini
sudo docker exec -it moodle sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/7.4/apache2/php.ini
sudo docker exec -it moodle sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/7.4/cli/php.ini
sudo docker exec -it moodle sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/8.1/cli/php.ini
sudo docker exec -it moodle sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/7.4/apache2/php.ini
sudo docker exec -it moodle service apache2 restart

# Variable untuk sistem Hazman
SERVER=`hostname -I | cut -d' ' -f1`

# Install Proses Flink
cd ~/favian-scele-datastream-system
sed -i "s/192.168.43.220/$SERVER/g" asynchronous-plugin-reader/src/main/resources/environment.properties
sed -i "s/192.168.43.220/$SERVER/g" asynchronous-plugin-reader/src/main/java/org/datastream/khodza/sink/influx/AsynchronousInfluxMap.java
cd ~/favian-scele-datastream-system/asynchronous-plugin-reader/
sudo mvn clean package

# Install Hazman Streaming Process
cd ~/favian-scele-datastream-system
sed -i "s/192.168.43.220/$SERVER/g" favian-warp-kafka/docker-compose.yml
cd ~/favian-scele-datastream-system
sudo docker network create warp-net
sudo docker-compose -f favian-warp-kafka/docker-compose.yml up -d
cd ~/favian-scele-datastream-system/favian-stacks-webserver/monitoring
sudo sh restart.sh
cd ~/favian-scele-datastream-system
sudo docker-compose -f favian-stacks-webserver/debezium/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/flink-cluster/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/influx/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/monitoring/docker-compose.yml up -d
sudo docker-compose -f favian-stacks-webserver/portainer/docker-compose.yml up -d

# Buat bridge debezium
bash ~/favian-scele-datastream-system/favian-stacks-webserver/debezium/refresh-connector.sh

# Buat database di influxdb
sudo docker exec influxdb influx -execute "CREATE DATABASE t_integration_learninganalytics"

# Masuk proses Flink ke dalam Flink
sudo docker exec flink-cluster_jobmanager_1 \
 ./bin/flink run --detached -c org.datastream.khodza.Khodza \
 asynchronous.jar org.datastream.khodza.job.Scele scele.s1.properties
