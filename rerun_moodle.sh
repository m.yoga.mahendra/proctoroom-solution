# Delete moodle yang lama
sudo docker rm -f moodle
sudo docker rmi -f adil/moodle:v6

# Build Image yang telah disesuaikan dengan VM
cd ~/proctor-room
sudo docker build --build-arg publicIpAddress=$(curl ifconfig.me) --build-arg dbhost='postgres' -t adil/moodle:v6 .

# Run 
sudo docker run \
 --name moodle -d -i -v moodledata:/var/moodledata \
  -p 80:80 -p 5000:5000 --network moodle-network \
  adil/moodle:v6

# Install Moodle
sudo docker exec -it moodle chown -R www-data /var/moodledata
sudo docker exec -it moodle chmod -R 777 /var/moodledata
sudo docker exec -it moodle chmod -R 777 /var/moodledata
sudo docker exec -it moodle sh install_moodle.sh
sudo docker exec -it moodle service apache2 start
sudo docker exec -it moodle nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > ~/websocket-output.log 2>&1 &
sudo docker exec -ti moodle sed -i "s|^.*examplepassword.*$|\$CFG->tool_generator_users_password = 'examplepassword';|" /var/www/html/moodle/config.php
sudo docker exec -ti moodle sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/7.4/cli/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/8.1/cli/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/7.4/apache2/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/7.4/cli/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/8.1/cli/php.ini
sudo docker exec -ti moodle sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/7.4/apache2/php.ini
sudo docker exec -ti moodle service apache2 restart

# Buat bridge debezium
cd ~/microservices-moodle
bash debezium/refresh-connector.sh