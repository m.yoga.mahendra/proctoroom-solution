# Setup Docker Swarm Join Worker
docker swarm join --token SWMTKN-1-0gxp83uhcv2zo9vbquv05xw78g3opu3jdstm704mpgrn699rj5-0cqjsgf27wh0az9vqhjz6zqw0 10.128.0.42:2377

# Clone repo konfig kafka
cd ..
git clone https://gitlab.com/m.yoga.mahendra/microservices-manager.git
cd ~/microservices-manager/

# Variable untuk sistem Hazman
SERVER=`hostname -I | cut -d' ' -f1`
sed -i "s/192.168.43.220/$SERVER/g" docker-compose-kafka-1.yml
sed -i "s/192.168.43.220/$SERVER/g" docker-compose-kafka-2.yml
sed -i "s/192.168.43.220/$SERVER/g" docker-compose-kafka-3.yml