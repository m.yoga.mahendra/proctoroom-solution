#!/bin/sh

# Variable untuk sistem Hazman
SERVER=`hostname -I | cut -d' ' -f1`

# Install Proses Flink
cd ~/microservices-streamingprocess
sed -i "s/192.168.43.220/$SERVER/g" asynchronous-plugin-reader/src/main/java/org/datastream/khodza/sink/influx/AsynchronousInfluxMap.java
cd ~/microservices-streamingprocess/asynchronous-plugin-reader/
sudo mvn clean package

cd ~/microservices-streamingprocess
sudo docker-compose -f flink-cluster/docker-compose.yml up --force-recreate -d
sudo docker-compose -f influx/docker-compose.yml up --force-recreate -d
sudo docker-compose -f monitoring/docker-compose.yml up --force-recreate -d

# Buat database di influxdb
sudo docker exec influxdb influx -execute "CREATE DATABASE t_integration_learninganalytics"

# Masuk proses Flink ke dalam Flink
sudo docker exec flink-cluster_jobmanager_1 \
 ./bin/flink run --detached -c org.datastream.khodza.Khodza \
 asynchronous.jar org.datastream.khodza.job.Scele scele.s1.properties