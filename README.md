# Proctor Room

Proctor Room adalah infrastruktur untuk melakukan pengawasan kepada peserta ujian yang menggunakan Moodle. Harapan dari infrastruktur ini adalah dapat dilakukan pencegahan kecurangan ujian.

# List Repositori
https://gitlab.com/m.yoga.mahendra/proctoroom-solution

https://gitlab.com/m.yoga.mahendra/favian-stacks-webserver

https://gitlab.com/m.yoga.mahendra/microservices-manager

https://gitlab.com/m.yoga.mahendra/microservices-moodle

https://gitlab.com/m.yoga.mahendra/microservices-streamingprocess


## Penjelasan Lebih Detail

1. [Video sidang](https://youtu.be/MFZmVBooDp4)
2. [Makalah skripsi](https://www.researchgate.net/publication/362057514_REAL_TIME_STREAM_PROCESSING_AKTIVITAS_UJIAN_PILIHAN_GANDA_PADA_FITUR_KUIS_DI_PLATFORM_MOODLE)

## Environment yang penulis gunakan

Penulis menjalankan Proctor Room dengan menggunakan [AWS EC 2 t2.large instance](https://aws.amazon.com/id/ec2/instance-types/). Berikut adalah spesifikasi dari environment tersebut:

- Ubuntu 18.04
- Storage 20 GB
- RAM 8 GB
- CPU 2 core

## Instalasi Proctor Room

Copy-paste hal-hal ini pada server tersebut

``` 
git clone https://gitlab.com/aulia-adil/proctor-room.git
cd proctor-room
sh run.sh

# Untuk menyalakan websocket
sudo docker exec -it moodle nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > ~/websocket-output.log 2>&1 & 

# Untuk mendapatkan root access docker

sudo usermod -aG docker $USER
newgrp docker
```

## Konfigurasi Default Password User Moodle
1. Masuk ke container moodle
sudo docker exec -ti moodle bash
2. Navigasi dengan ```cd /var/www/html/moodle```, lalu jalankan perintah berikut
```
sed -i "s|^.*examplepassword.*$|\$CFG->tool_generator_users_password = 'examplepassword';|" /var/www/html/moodle/config.php
```
Jangan lupa naikkan max execution time pada config.php
```
sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/7.4/cli/php.ini
sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/8.1/cli/php.ini
sed -i "s|^.*max_execution_time = .*$|max_execution_time = 240|" /etc/php/7.4/apache2/php.ini
sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/7.4/cli/php.ini
sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/8.1/cli/php.ini
sed -i "s|^.*max_input_time = .*$|max_input_time = 240|" /etc/php/7.4/apache2/php.ini
```
3. Jalankan perintah berikut untuk merestart service apache2
```
service apache2 restart
```
## Open Grafana untuk cek keberhasilan

1. Buka Grafana: http://public-ip-address:3000
Pada saat penulisan ini, Grafana memiliki username dan password default: admin
1. Add data source InfluxDB dengan URL private-ip-address:8086 , database name: `t_integration_learninganalytics` , HTTP Method GET. Perlu diingat private-ip-address di sini bukanlah localhost.
2. Dengan menggunakan Grafana, lakukan query kepada influxDB: `SELECT * FROM "t-scele-asynchronous"`

## Jika membuka VM yang sudah dimatikan

Lakukan: 
```
sh rerun.sh

# Untuk menyalakan websocket (sudah ada di dalam rerun.sh)
sudo docker exec -it moodle nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > ~/websocket-output.log 2>&1 & 
```

## Membuat Load Testing

1. Masuk ke docker Moodle dengan command: `sudo docker exec -ti f3859 bash`
2. Pergi ke `config.php` dari Moodle dengan command: `cd /var/www/html/moodle/`. Di sana ada `config.php`
3. Install vim atau yang semacamnya: `apt update && apt install vim`
4. Konfigurasi `config.php` dengan `vim config.php`
5. Cari `examplepassword` dengan `/` di VIM
6. Uncomment line of code tersebut
7. Restart Apache2 dengan `service apache2 restart`
8. Pergi ke Moodle dan aktifkan Developer mode. Centang juga display debug message dan performance info.
9. Buat kelas yang berisi `n` murid menggunakan make test course, tunggu hingga selesai
10. Pergi ke JMeter test plan
11. Buat test plan dengan `n` murid tersebut, atau sesuai kebutuhan (pilihannya: 1,30,100,1000 dst murid)
12. Checklist update password
13. Submit dan download data muridnya
14. Jika ada eror seperti berikut:
```Fatal error: Maximum execution time of 30 seconds exceeded in /var/www/html/moodle/lib/moodlelib.php on line 4679```
maka ubah nilai variabel max_execution_time dan max_input_time dalam berkas php.ini pada baris ~409 di lokasi berikut:
```
root@d6d5afa902ff:/# find -O3 /* -name "php.ini"
/etc/php/8.1/cli/php.ini
/etc/php/7.4/cli/php.ini
/etc/php/7.4/apache2/php.ini
```
Setelah itu, restart Apache2 dengan `service apache2 restart` dan kembali ke langkah 11.

# Urutan menghidupkan atau menjalankan mesin microservices
1. Manager
2. Kafka 1-3
3. Moodle
4. Streaming Process