# Delete moodle yang lama
sudo docker rm -f moodle
sudo docker rmi -f adil/moodle:v6

# Build Image yang telah disesuaikan dengan VM
cd ~/proctor-room
sudo docker build --build-arg publicIpAddress=$(curl ifconfig.me) --build-arg dbhost='postgres' -t adil/moodle:v6 .

# Run 
sudo docker run \
 --name moodle -d -i -v moodledata:/var/moodledata \
  -p 80:80 -p 5000:5000 --network moodle-network \
  adil/moodle:v6

# Install ulang Moodle
sudo docker exec -it moodle chown -R www-data /var/moodledata
sudo docker exec -it moodle chmod -R 777 /var/moodledata
sudo docker exec -it moodle chmod -R 777 /var/moodledata
sudo docker exec -it moodle sh install_moodle.sh
sudo docker exec -it moodle service apache2 start
sudo docker exec -it moodle nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > ~/websocket-output.log 2>&1 &

# Buat bridge debezium
bash ~/favian-scele-datastream-system/favian-stacks-webserver/debezium/refresh-connector.sh

# Masuk proses Flink ke dalam Flink
sudo docker exec flink-cluster_jobmanager_1 \
 ./bin/flink run --detached -c org.datastream.khodza.Khodza \
 asynchronous.jar org.datastream.khodza.job.Scele scele.s1.properties

